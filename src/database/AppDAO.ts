module MyGame {
    export  class AppDao {
        db : any

        constructor (){
            this.db = sqlitePlugin.openDatabase({
                name:'edu_school.db',
                location:'default',
                androidDatabaseImplementation:1
            })
        }

        run(sql : string,args? : string[]) {
            return new Promise((resolve, reject) => {
                this.db.transaction((tx : SQLitePlugin.Database) => {
                    tx.executeSql(sql,args,
                        ((tx? :any,res?:any) => {
                        resolve(res)
                    }), ((tx? : any, err? : Error) => {
                        reject(err)
                    }))
                },((err? : Error) => {
                }))

            })
        }

        get(sql : string , params? : string[]) {
            return new Promise((resolve, reject) => {
                this.db.transaction((tx : SQLitePlugin.Database) => {
                    tx.executeSql(sql,params,
                        ((tx? :any,res?:any) => {
                        console.log(tx,res)
                        resolve(res)
                    }), ((tx? : any, err? : Error) => {
                        console.log(tx,err)
                        reject(err)
                    }))
                },((err? : Error) => {
                    console.log(err)
                }))
            })
        }
    }
}
module MyGame {
    export  class Users {
    dao : any
    constructor(dao : any) {
        this.dao = dao
    }
    createTable() {
        let sql = `CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, idToken TEXT, level INTEGER DEFAULT 1, achievements TEXT DEFAULT '', isPremium INTEGER DEFAULT 0)`
        return this.dao.run(sql, [])
    }

    create(name : any, idToken? : string, level? : number, achievements? : string,isPremium?:boolean) {
        return this.dao.run(
            'INSERT INTO users (name, idToken ,level ,achievements, isPremium ) VALUES (?,?,?,?,?)',
            [name, idToken,level,achievements,isPremium]
        )
    }

    updateAchievements(achievements : string, name : string) {
        return this.dao.run(
            `UPDATE users SET achievements = ? WHERE name = ?`,
            [achievements, name]
        )
    }

    delete(id : number) {
        return this.dao.run(
            `DELETE FROM users WHERE id = ?`,
            [id]
        )
    }

    // getToken(id : number) {
    //     return this.dao.get(
    //         `SELECT idToken FROM users WHERE id = ?`,
    //         [id]
    //     )
    // }

    getById(id : number) {
        return this.dao.run(
            `SELECT * FROM users WHERE id = ?`,
            [id]
        )
    }

    getAchievements(achievements : string){
        return this.dao.run(
            `SELECT achievements FROM users WHERE instr(achievements, ?)`,
            [achievements]
        )
    }

    checkTable(tableName : string) {
        return this.dao.run(
            `SELECT count(*) FROM users WHERE type='table' AND name = ?`,
            [tableName]
        )
    }

    // getAll() {
    //     return this.dao.all(`SELECT * FROM users`)
    // }
    }
}
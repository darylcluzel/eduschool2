module MyGame {
    export class Settings {  
        dao : any
        constructor(dao : any) {
            this.dao = dao
        }

        createTable() {
            const sql = `
            CREATE TABLE IF NOT EXISTS settings (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT,
                description TEXT,
                isActive INTEGER DEFAULT 0,
                value TEXT
                )`

            return this.dao.run(sql)
        }
        create(name : string, description : string, isActive : number, value : string) {
            return this.dao.run(
                `INSERT INTO settings (name, description, isActive, value)
                    VALUES (?, ?, ?, ?)`,
                [name, description, isActive, value])
        }

        update(task : any) {
            const { id, name, description, isActive, value } = task
            return this.dao.run(
                `UPDATE settings
                SET name = ?,
                    description = ?,
                    isActive = ?,
                    value = ?
                WHERE id = ?`,
                [name, description, isActive, value, id]
            )
        }

        delete(id : number) {
            return this.dao.run(
                `DELETE FROM settings WHERE id = ?`,
                [id]
            )
        }

        getById(id : number) {
            return this.dao.get(
                `SELECT * FROM settings WHERE id = ?`,
            [id])
        }
    }
}
module MyGame {
	export  class MainMenu extends Phaser.State {
/**
 * Typing props 
 */
		inputName : any
		inputPwd :any
		background: Phaser.Sprite
		logo: Phaser.Sprite
		music: Phaser.Sound
		username : string
		password : string
		title : any
		btnStart : Phaser.Sprite
		Server : MyGame.Server

		create() {

			// this.game.debug.phaser(this.auth, 32, 32);
			this.music = this.add.audio('titleMusic')
			this.music.play()

			this.background = this.add.sprite(0, 0, 'titlepage')
			this.background.width = 360
			this.background.height = 640
			this.background.alpha = 0
			this.logo = this.add.sprite(this.world.centerX, -300, 'logo')
			this.logo.width = 196,2
			this.logo.height = 87,4
			this.logo.anchor.setTo(0.5, 0.5)

			this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true)
			this.title = this.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000)
			// console.log(this.title)
			// console.log(this.startChecking)
			this.startChecking()
			this.btnStart = this.add.sprite(this.world.centerX, -600, 'btn')
			// console.log(this.onComplete)
			// this.inputName = new Input(this.game,40 ,this.game.height/2 -130, 220, 30,"Veuillez renseigner votre nom") 
			// this.inputName = new Input(this.game,40 ,this.game.height/2 -70, 220, 30,"Veuillez renseigner un mot de passe")
			/**
			 * 	(localStorage) ? this.auth = new Auth() : this.auth() = new Server(localStorage.username, localStorage.password)
			 * 
			 * 
			 */
			// this.input.onDown.addOnce(this.fadeOut, this)

		}
		/**
		 * Animate an sprite,logo,element.. to fadeOut
		 */
		startChecking() {
			this.title.onComplete.add(this.onComplete,this, 0, {
				message : "Veuillez renseigner votre identifiant",
				title : "Authentification",
				inputName : "username"
			})
		}

		/*****
		 * Fonction up level for Cordova Navigator Notification Plug-in 
		 * @param title The Tween Chain launching prompts
		 * @param context Tween Context
		 * @param data args passed on Complete tweening
		 */
		onComplete(title : Phaser.Sprite,context : any, data : any) {
			navigator.notification.prompt(
				data.message,
				(results) => {
					this.callback(results, data.inputName)
				},
				data.title,
				data.btn,
				data.text
			)
			console.log('prompt passed')
		}


		/**
		 * 
		 * @param results Object containing ButtonIndex (The index of button clicked) & input1 (Text entered in prompt)
		 * @param inputName The Input Type
		 */
		callback(results : any, inputName : string) {
			console.log(inputName)
			if (inputName === "username") 
			{
				this.username = results.input1
				this.onComplete(this.title, 0, {
					message : "Veuillez renseigner votre password",
					title : "Authentification",
					inputName : "password"
				})
			// this.onComplete,this, 1, {message : "Veuillez renseigner votre mot de passe",title : "Authentification", inputName : "password"}
				this.username = results.input1
			} else {
				
				this.password = results.input1
				// let server = new Server({"username" : this.username,"password" : this.password})
				let res = fetch('http://51.15.237.59:8080/auth', {
					method: 'POST',
					headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
					body: JSON.stringify({
						username: this.username,
						password: this.password
					})
				})
				.then((response) => {
					console.log(response)
					response.json()
				})
				.then((responseData) => {
					console.log(responseData)
					return this.fadeOut()
				})
				.catch((err) => {
					console.log(err)
					this.startChecking()
				})
				console.log(res)
			}
		}

		fadeOut() {

			this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
			var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Bounce.In, true);

			tween.onComplete.add(this.getMain, this);

		}
		/**
		 * Start the game
		 */
		getMain() {
			// Faire le check si y'a un localStorage si oui skip l'étape Auth;
			this.game.state.start('ParentAdmin', true, false);

		}
		
	}
}
module MyGame {
	export  class Boot extends Phaser.State {
		/**
		 * Create all the canvas & display settings
		 */
		init() {
			//  Support multiTouch
			this.input.maxPointers = 1;

			//  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
			this.stage.disableVisibilityChange = true;

			// Enable physics
			this.game.physics.startSystem(Phaser.Physics.ARCADE);

			if (this.game.device.desktop) {
				//  If you have any desktop specific settings, they can go in here
				this.game.scale.pageAlignHorizontally = true;
				this.game.scale.pageAlignVertically = true;
			}
			else {
				//  Same goes for mobile settings.
				this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
				// this.scale.minWidth = 320;
				// this.scale.minHeight = 480;
				// this.scale.maxWidth = 768;
				// this.scale.maxHeight = 1334;
			}

		}
		/**
		 * Phaser method to create a preload bar
		 */
		preload() {
			this.load.image('preloadBar', 'assets/loader.png');
		}

		create() {
			//  By this point the preloader assets have loaded to the cache, we've set the game settings
			//  So now let's start the real preloader going
			this.game.state.start('Preloader');
		}
	}
}
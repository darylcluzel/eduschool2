module MyGame {
        export  class Gems extends Phaser.Sprite {


        gem : Phaser.Sprite;
        gemMaxNumber : number;
        total : 0;
        /** 
        * @param game The Game instance of the actual
        * @param x X position
        * @param y Y position 
        * @param gemMaxNumber set the MaxGemsNumber when class is called
        */
        constructor(game: Phaser.Game, x: number, y: number, gemMaxNumber : number) {

            super(game, x, y, 'gem', 0);

            this.game.physics.arcade.enableBody(this);
            this.width = 29;
            this.height = 31;
            this.anchor.setTo(.5);
            this.gemMaxNumber = gemMaxNumber;
            this.body.immovable = true;

            this.game.add.existing(this);

        }
        /**
         * Create gems to the maxGemsNumber
         */
        releaseGems(){
            if(this.gemMaxNumber > this.total){
                this.gem = this.game.add.sprite((Math.floor(320)),this.game.world.randomY,'gem');
                
                this.total++;
            } else {

            }
            
        }

    }
}
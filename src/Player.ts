module MyGame {
	export  class Player extends Phaser.Sprite {
		/**
		 * 
		 * @param game Phaser.Game instance
		 * @param x X position
		 * @param y Y position
		 * @param frame the original frame displayed
		 */
		constructor(game: Phaser.Game, x: number, y: number, frame : number) {

			super(game, x, y, 'player', frame);
			//Setting up basics config
			this.game.physics.arcade.enableBody(this);
			this.width = 43.5;
			this.height = 55;
			this.anchor.setTo(.5);
			//Add Animation on Sprite
			this.animations.add('runR',[9,10,11],60);
			this.animations.add('runL',[24,25,26],60);
			this.animations.add('forward',[22],90);
			this.animations.add('backward',[0],90);
			this.animations.add('jump',[3,8],70);

			game.add.existing(this);
		}
		/**
		 * Phaser method call 60times/sec
		 */
		update() {
			this.body.velocity.y = 0;
			this.body.velocity.x = 0;
			// RIGHT & LEFT ACTIONS
			if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {

				this.body.velocity.x = -150;
				this.animations.play('runL');

				if (this.scale.x == 1) {
					this.scale.x = -1;
				}
			}
			else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {

				this.body.velocity.x = 150;
				this.animations.play('runR');

				if (this.scale.x == -1) {
					this.scale.x = 1;
				}
			}
			// Frame to display if nothing is pressed or anything else than LEFT RIGHT TOP DOWN
			else {
				this.animations.frame = 22;
			}
			// TOP & DOWN ACTIONS
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
				this.body.velocity.y = -100;
				this.animations.play('forward');
			}
			else if (this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
				this.body.velocity.y = 100;
				this.animations.play('backward');
			}

		}

	}
}
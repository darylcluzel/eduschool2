module MyGame {
        export  class ParentAdmin extends Phaser.State {


        //Type Button
        btnAddOn : Phaser.Image
        btnAddChild : Phaser.Image
        btnChildAccount : Phaser.Image
        btnResult : Phaser.Image
        btnBottom : Phaser.Image
        btnPlay : Phaser.Image
        clickButtonPlay : any

        background : Phaser.Image
        panel : Phaser.Image
        panelTitle : Phaser.Image
        txtBtn : Phaser.Text
        btnGroup : Phaser.Group
        style : any

        preload(){
            this.style = {
                font : "18px Arial",
                fill: '#FFF',
                wordWrap: true,
                align: "center"
            }
        }

        create(){
            this.background = this.add.sprite(0, 0, 'titlepage')
			this.background.width = 360
            this.background.height = 640
            

            //Panel
            this.panel = this.add.image(50,100,'panel','Panel')
            this.panel.width = 260
            this.panel.height = 390
            this.panelTitle = this.add.image(80, 80, 'panelTitle','Panel')
            this.panelTitle.width = 200
            this.panelTitle.height = 50

            // Buttons group
            this.btnChildAccount = this.add.image(100,170,'btnChildAccount','Button')
            this.btnChildAccount.width = 160
            this.btnChildAccount.height = 60

            this.btnResult = this.add.image(100,240,'btnResult','Button')
            this.btnResult.width = 160
            this.btnResult.height = 60
            
            this.btnAddOn = this.add.image(100,320,'btnAddOn','Button')
            this.btnAddOn.width = 160
            this.btnAddOn.height = 60

            this.btnAddChild = this.add.image(85,400,'btnAddChild','Button')
            this.btnAddChild.width = 190
            this.btnAddChild.height = 60

            this.btnBottom = this.add.image(45,520,'btnBottom','ButtonPlay')
            this.btnBottom.width = 270
            this.btnBottom.height = 60
            this.clickButtonPlay = this.game.add.button(135,500,'btnPlay',this.startAddition,this)
            // this.btnPlay = this.add.image(135,500,'btnPlay','ButtonPlay')
            this.clickButtonPlay.width = 90
            this.clickButtonPlay.height = 90

            // this.btnPlay.events.onInputDown.add((elem : string)=> {
            //     this.btnPlay.scale.setTo(.1,.1)
            //     console.log(elem)
            //     this.startAddition
            // },this)



            
        }

        update(){

        }

        startAddition(){
            this.state.start('Addition', true, false)
        }

        makeBtn(x : number, y : number ,name : string, group : string, text? : string) {
            // this.add.image
        }


    }
}
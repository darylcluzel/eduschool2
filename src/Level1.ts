module MyGame {
	export  class Level1 extends Phaser.State {

		/**
		 * Typing props
		 */
		background: Phaser.TileSprite;
		image : Phaser.Image;
		music: Phaser.Sound; 
		player: MyGame.Player;
		gems : MyGame.Gems; 
		maxGems : 3;
		totalGem : 0;
		gemGroup : Phaser.Group;

		/**
		 * Phaser 1st method called
		 */
		create() {
			this.image = this.add.image(0, 0, 'gem');
			this.background = this.game.add.tileSprite(0, 0, this.game.stage.width, this.game.cache.getImage('bg_game').height, 'bg_game');
			// this.background = this.add.sprite(0, 0, 'bg_game');
			this.gems = new Gems(this.game, this.getRandomInt(10, 320), this.getRandomInt(10, 600) , this.maxGems);
				
			this.game.debug.spriteInfo(this.gems, 32, 32);
				
			this.player = new Player(this.game, 80, 700, 22);
			this.game.physics.enable(this.gems, Phaser.Physics.ARCADE);
			
			let lvl = new Level1();
			let x = Describer.describe(lvl);
			console.log(x);
		}
		/**
		 * Phaser method call 60times/sec
		 */
		update() {
			this.background.tilePosition.y += 1.5;
			if(this.totalGem < this.maxGems ){ 
				this.gemGroup.add(this.gems.releaseGems());
				this.totalGem++;
			} else {

			}
			
			this.game.physics.arcade.collide(this.gems, this.player);
			this.game.debug.spriteInfo(this.gems,54,54);
			this.gems.position.y += 1;
			this.gems.angle += 3;
		}
		/**
		 * Get a random value between min-max
		 * 
		 * @param min Min value to random
		 * @param max Max value to random
		 */
		getRandomInt(min : number, max : number) {
			return Math.floor(Math.random() * (max - min + 1) + min);
		}
	}
}
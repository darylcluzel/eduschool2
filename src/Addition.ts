module MyGame {
    // "PlayGame" scene
    export class Addition extends Phaser.State {
        gameOptions = {
    
            // maximum length of the sum
            maxSumLen: 5,

            // time allowed to answer a question, in milliseconds
            timeToAnswer: 3000,
        
            // score needed to increase difficulty
            nextLevel: 400
        } 
        isGameOver : boolean
        score : number
        correctAnswers : number
        sumsArray : any
        questionText : any
        scoreText : any
        buttonMask : any
        topScore : number
        timeTween : Phaser.Tween
        randomSum : number
        numberTimer : any

        preload(){
        }
    
        create(){
            const dao = new AppDao()
            const daoUser = new Users(dao)
            const res = daoUser.getAchievements('additionTopScore').then((data : any, rows : any) => {
                console.log(data,rows)
            })
            console.log(res)

            this.buttonMask = this.add.image(80,400,'buttonmask')
            this.isGameOver = false;
            this.score = 0;
            this.topScore = 0
            this.correctAnswers = 0;
            this.sumsArray = []
            this.stage.backgroundColor = "#e0e4f1"
            //Create random questions
            for(var i = 1; i < this.gameOptions.maxSumLen; i++){
                this.sumsArray[i]=[[], [], []];
                for(var j = 1; j <= 3; j++){
                    this.buildThrees(j, 1, i, j);
                }
            }
            console.log(this.sumsArray);
            this.questionText = this.game.add.text(180 , 160, "-", {
                font: "bold 68px Arial"
            });
            this.questionText.anchor.set(0.5);
            this.scoreText = this.add.text(10, 10, "-", {
                font: "bold 24px Arial"
            });
            for(i = 0; i < 3; i++){
                var numberButton = this.add.button(80, 400 + i * 75, "buttons",this.checkAnswer,this)
                numberButton.frame = i;
                numberButton.width = 200
            }
    
            // adding the time bar
            this.numberTimer =  this.add.sprite(80, 400, "timebar");
            this.numberTimer.width = 200
            this.nextNumber();
        }
        
        //Algo
        buildThrees(initialNummber : number, currentIndex : number, limit : number, currentString : any){
    
            var numbersArray = [-3, -2, -1, 1, 2, 3];
    
            for(var i = 0; i < numbersArray.length; i++){
    
                var sum = initialNummber + numbersArray[i];
    
                var outputString = currentString + (numbersArray[i] < 0 ? "" : "+") + numbersArray[i];
    
                if(sum > 0 && sum < 4 && currentIndex == limit){
    
                    this.sumsArray[limit][sum - 1].push(outputString);
                }
    
                if(currentIndex < limit){
    
                    this.buildThrees(sum, currentIndex + 1, limit, outputString);
                }
            }
        }
    
        // this method asks next question
        nextNumber(){
    
            // updating score text
            this.scoreText.setText("Score: " + this.score.toString() + "\nBest Score: " + this.topScore.toString());
            if(this.buttonMask) {
                this.buttonMask.destroy()
            }
            this.buttonMask = this.add.graphics(80, 400);
            this.buttonMask.beginFill(0xffffff);
            this.buttonMask.drawRect(0, 0, 200, 200);
            this.buttonMask.endFill();
            this.numberTimer.mask = this.buttonMask
            if(this.correctAnswers > 1){

                // stopping time tween
                this.timeTween.stop();
                // resetting mask horizontal position
                this.buttonMask.x = 80;
            }
            if(this.correctAnswers > 0){
    
                this.timeTween = this.add.tween(this.buttonMask).to({x: -200}, 3000,'Linear',true)
                this.timeTween.onComplete.add(this.gameOver, this,0,"?")
            }
            this.randomSum = Phaser.Math.between(0, 2);
    
            var questionLength = Math.min(Math.floor(this.score / this.gameOptions.nextLevel) + 1, 4)
    
            // updating question text
            this.questionText.setText(this.sumsArray[Math.min(Math.round((this.score-100)/400)+1,4)][this.randomSum][this.rnd.between(0,this.sumsArray[Math.min(Math.round((this.score-100)/400)+1,4)][this.randomSum].length-1)])
        }	
        // method to check the answer, the argument is the button pressed
        checkAnswer(button : Phaser.Button){
    
            // we check the answer only if it's not game over yet
            if(!this.isGameOver){
    
                // button frame is equal to randomSum means the answer is correct
                if(button.frame == this.randomSum){
    
                    // score is increased according to the time spent to answer
                    this.score += Math.floor((this.buttonMask.x + 350) / 4);
    
                    // one more correct answer
                    this.correctAnswers++;
    
                    // moving on to next question
                    this.nextNumber();
                }
    
                else{
    
                    if(this.correctAnswers > 1) {
    
                        this.timeTween.stop();
                    }
                    this.gameOver(button.frame.toString() +"1");
                }
            }
        }
    
        gameOver(gameOverString : string){
            this.stage.backgroundColor = "#ff0000"
            // displaying game over text
            this.questionText.setText(this.questionText.text + " = " + gameOverString);
    
            this.isGameOver = true;
            const dao = new AppDao()
			const daoUser = new Users(dao)
            daoUser.updateAchievements("daryl","additionTopScore:"+Math.max(this.score,this.topScore))
            // restart the game after two seconds
            this.time.events.add(2000,() => {
                    this.state.start('Addition', true, false);
                },this
            );
        }
    }
}

module MyGame {
	export const gameWidth = window.innerWidth * window.devicePixelRatio
	export const gameHeight = window.innerHeight * window.devicePixelRatio
	export const gameConfig = {
        width: 360,
        height: 640,
    }
	export  class Game extends Phaser.Game {

	plugin : any;
	/**
	 * Create Phaser state to access anywhere
	 */
	constructor() {
		super(gameConfig.width,gameConfig.height,Phaser.WEBGL, 'content', {enableDebug: false});
		// this.plugin = new PhaserInput.Plugin(this, this.plugins);
		this.state.add('Boot', Boot, false);
		this.state.add('Preloader', Preloader, false);
		this.state.add('MainMenu', MainMenu, false);
		this.state.add('Level1', Level1, false);
		this.state.add('Server', Server, false);
		this.state.add('ParentAdmin', ParentAdmin, false)
		this.state.add('Addition', Addition, false)
		// Start chaining files
		this.state.start('Boot');
	}

	}

	var app = {
		// Application Constructor
		initialize: function() {
			document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
		},

		// deviceready Event Handler
		//
		// Bind any cordova events here. Common events are:
		// 'pause', 'resume', etc.
		onDeviceReady: function() {
			this.receivedEvent('deviceready');
		},

		// Update DOM on a Received Event
		receivedEvent: function(id : string) {
			new Game;
		}
	};
	app.initialize();
}
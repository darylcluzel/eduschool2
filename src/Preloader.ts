module MyGame {
	export  class Preloader extends Phaser.State {

		preloadBar: Phaser.Sprite;
		background: Phaser.Sprite;
		ready: boolean = false;
		
		preload() {
			//	These are the assets we loaded in Boot.js
			this.preloadBar = this.add.sprite(300, 400, 'preloadBar');

			//	This sets the preloadBar sprite as a loader sprite.
			//	What that does is automatically crop the sprite from 0 to full-width
			//	as the files below are loaded in.
			this.load.setPreloadSprite(this.preloadBar);

			//	Here we load the rest of the assets our game needs.
			//	As this is just a Project Template I've not provided these assets, swap them for your own.
			this.load.image('titlepage', 'assets/titlepage.jpg');
			this.load.audio('titleMusic', 'assets/title.mp3', true);
			this.load.image('logo', 'assets/logo.png');
			this.load.spritesheet('player', 'assets/player.png', 79.8, 108.3 , 27);
			this.load.spritesheet('bg_game', 'assets/bg_game_1.png', 1080, 1920);
			this.load.spritesheet('gem', 'assets/gem.png', 79,83);
			this.load.image('panel', 'assets/Panel.png')
			this.load.image('btn', 'assets/btn.png')
			this.load.image('panelTitle', 'assets/base.png')
			this.load.image("timebar", "assets/timebar.png")
			this.load.image("btnAddChild", "assets/btnAdmin/add_child.png")
			this.load.image("btnAddOn", "assets/btnAdmin/btn_add_on.png")
			this.load.image("btnBottom", "assets/btnAdmin/btn_bottom_child.png")
			this.load.image("btnChildAccount", "assets/btnAdmin/btn_child_account.png")
			this.load.image("btnResult", "assets/btnAdmin/btn_result.png")
			this.load.image("btnPlay", "assets/btnAdmin/play_btn.png")
            this.load.image('buttonmask', "assets/buttonmask.png")
			this.load.spritesheet("buttons", "assets/buttons.png", 400, 50, 3)

			let db = this.db()
			console.log(db)
			//	+ lots of other required assets here

		}

		db() {
			const dao = new AppDao()
			const daoUser = new Users(dao)
			const daoSettings = new Settings(dao)

			daoUser.createTable()
			.then(() => daoSettings.createTable())
			.then((data : any) => {
				daoSettings.create("Theme","Paramètrage du thème",0,"")
				console.log("create",data)
			})
			.then(() => daoUser.create("Daryl","1"))
			// .then((res : any ) => {
			// 	daoUser.checkTable('name')
			// 	console.log("checkTable",res)
			// })
			.then((data : any) => {
				console.log(data)
			})
			.catch((err:string) => {
				console.log('Error: ',err)
				console.log(JSON.stringify(err))
			})
		}

		create() {
			this.game.state.start('MainMenu');
		}
	}
}
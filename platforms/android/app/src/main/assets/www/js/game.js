var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var MyGame;
(function (MyGame) {
    var Addition = (function (_super) {
        __extends(Addition, _super);
        function Addition() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.gameOptions = {
                maxSumLen: 5,
                timeToAnswer: 3000,
                nextLevel: 400
            };
            return _this;
        }
        Addition.prototype.preload = function () {
        };
        Addition.prototype.create = function () {
            var dao = new MyGame.AppDao();
            var daoUser = new MyGame.Users(dao);
            var res = daoUser.getAchievements('additionTopScore').then(function (data, rows) {
                console.log(data, rows);
            });
            console.log(res);
            this.buttonMask = this.add.image(80, 400, 'buttonmask');
            this.isGameOver = false;
            this.score = 0;
            this.topScore = 0;
            this.correctAnswers = 0;
            this.sumsArray = [];
            this.stage.backgroundColor = "#e0e4f1";
            for (var i = 1; i < this.gameOptions.maxSumLen; i++) {
                this.sumsArray[i] = [[], [], []];
                for (var j = 1; j <= 3; j++) {
                    this.buildThrees(j, 1, i, j);
                }
            }
            console.log(this.sumsArray);
            this.questionText = this.game.add.text(180, 160, "-", {
                font: "bold 68px Arial"
            });
            this.questionText.anchor.set(0.5);
            this.scoreText = this.add.text(10, 10, "-", {
                font: "bold 24px Arial"
            });
            for (i = 0; i < 3; i++) {
                var numberButton = this.add.button(80, 400 + i * 75, "buttons", this.checkAnswer, this);
                numberButton.frame = i;
                numberButton.width = 200;
            }
            this.numberTimer = this.add.sprite(80, 400, "timebar");
            this.numberTimer.width = 200;
            this.nextNumber();
        };
        Addition.prototype.buildThrees = function (initialNummber, currentIndex, limit, currentString) {
            var numbersArray = [-3, -2, -1, 1, 2, 3];
            for (var i = 0; i < numbersArray.length; i++) {
                var sum = initialNummber + numbersArray[i];
                var outputString = currentString + (numbersArray[i] < 0 ? "" : "+") + numbersArray[i];
                if (sum > 0 && sum < 4 && currentIndex == limit) {
                    this.sumsArray[limit][sum - 1].push(outputString);
                }
                if (currentIndex < limit) {
                    this.buildThrees(sum, currentIndex + 1, limit, outputString);
                }
            }
        };
        Addition.prototype.nextNumber = function () {
            this.scoreText.setText("Score: " + this.score.toString() + "\nBest Score: " + this.topScore.toString());
            if (this.buttonMask) {
                this.buttonMask.destroy();
            }
            this.buttonMask = this.add.graphics(80, 400);
            this.buttonMask.beginFill(0xffffff);
            this.buttonMask.drawRect(0, 0, 200, 200);
            this.buttonMask.endFill();
            this.numberTimer.mask = this.buttonMask;
            if (this.correctAnswers > 1) {
                this.timeTween.stop();
                this.buttonMask.x = 80;
            }
            if (this.correctAnswers > 0) {
                this.timeTween = this.add.tween(this.buttonMask).to({ x: -200 }, 3000, 'Linear', true);
                this.timeTween.onComplete.add(this.gameOver, this, 0, "?");
            }
            this.randomSum = Phaser.Math.between(0, 2);
            var questionLength = Math.min(Math.floor(this.score / this.gameOptions.nextLevel) + 1, 4);
            this.questionText.setText(this.sumsArray[Math.min(Math.round((this.score - 100) / 400) + 1, 4)][this.randomSum][this.rnd.between(0, this.sumsArray[Math.min(Math.round((this.score - 100) / 400) + 1, 4)][this.randomSum].length - 1)]);
        };
        Addition.prototype.checkAnswer = function (button) {
            if (!this.isGameOver) {
                if (button.frame == this.randomSum) {
                    this.score += Math.floor((this.buttonMask.x + 350) / 4);
                    this.correctAnswers++;
                    this.nextNumber();
                }
                else {
                    if (this.correctAnswers > 1) {
                        this.timeTween.stop();
                    }
                    this.gameOver(button.frame.toString() + "1");
                }
            }
        };
        Addition.prototype.gameOver = function (gameOverString) {
            var _this = this;
            this.stage.backgroundColor = "#ff0000";
            this.questionText.setText(this.questionText.text + " = " + gameOverString);
            this.isGameOver = true;
            var dao = new MyGame.AppDao();
            var daoUser = new MyGame.Users(dao);
            daoUser.updateAchievements("daryl", "additionTopScore:" + Math.max(this.score, this.topScore));
            this.time.events.add(2000, function () {
                _this.state.start('Addition', true, false);
            }, this);
        };
        return Addition;
    }(Phaser.State));
    MyGame.Addition = Addition;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Boot.prototype.init = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            if (this.game.device.desktop) {
                this.game.scale.pageAlignHorizontally = true;
                this.game.scale.pageAlignVertically = true;
            }
            else {
                this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
            }
        };
        Boot.prototype.preload = function () {
            this.load.image('preloadBar', 'assets/loader.png');
        };
        Boot.prototype.create = function () {
            this.game.state.start('Preloader');
        };
        return Boot;
    }(Phaser.State));
    MyGame.Boot = Boot;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Describer = (function () {
        function Describer() {
        }
        Describer.describe = function (instance) {
            return Object.getOwnPropertyNames(instance);
        };
        return Describer;
    }());
    MyGame.Describer = Describer;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Emitter = (function (_super) {
        __extends(Emitter, _super);
        function Emitter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Emitter;
    }(Phaser.Group));
    MyGame.Emitter = Emitter;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    MyGame.gameWidth = window.innerWidth * window.devicePixelRatio;
    MyGame.gameHeight = window.innerHeight * window.devicePixelRatio;
    MyGame.gameConfig = {
        width: 360,
        height: 640
    };
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            var _this = _super.call(this, MyGame.gameConfig.width, MyGame.gameConfig.height, Phaser.WEBGL, 'content', { enableDebug: false }) || this;
            _this.state.add('Boot', MyGame.Boot, false);
            _this.state.add('Preloader', MyGame.Preloader, false);
            _this.state.add('MainMenu', MyGame.MainMenu, false);
            _this.state.add('Level1', MyGame.Level1, false);
            _this.state.add('Server', MyGame.Server, false);
            _this.state.add('ParentAdmin', MyGame.ParentAdmin, false);
            _this.state.add('Addition', MyGame.Addition, false);
            _this.state.start('Boot');
            return _this;
        }
        return Game;
    }(Phaser.Game));
    MyGame.Game = Game;
    var app = {
        initialize: function () {
            document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        },
        onDeviceReady: function () {
            this.receivedEvent('deviceready');
        },
        receivedEvent: function (id) {
            new Game;
        }
    };
    app.initialize();
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Gems = (function (_super) {
        __extends(Gems, _super);
        function Gems(game, x, y, gemMaxNumber) {
            var _this = _super.call(this, game, x, y, 'gem', 0) || this;
            _this.game.physics.arcade.enableBody(_this);
            _this.width = 29;
            _this.height = 31;
            _this.anchor.setTo(.5);
            _this.gemMaxNumber = gemMaxNumber;
            _this.body.immovable = true;
            _this.game.add.existing(_this);
            return _this;
        }
        Gems.prototype.releaseGems = function () {
            if (this.gemMaxNumber > this.total) {
                this.gem = this.game.add.sprite((Math.floor(320)), this.game.world.randomY, 'gem');
                this.total++;
            }
            else {
            }
        };
        return Gems;
    }(Phaser.Sprite));
    MyGame.Gems = Gems;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Level1 = (function (_super) {
        __extends(Level1, _super);
        function Level1() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Level1.prototype.create = function () {
            this.image = this.add.image(0, 0, 'gem');
            this.background = this.game.add.tileSprite(0, 0, this.game.stage.width, this.game.cache.getImage('bg_game').height, 'bg_game');
            this.gems = new MyGame.Gems(this.game, this.getRandomInt(10, 320), this.getRandomInt(10, 600), this.maxGems);
            this.game.debug.spriteInfo(this.gems, 32, 32);
            this.player = new MyGame.Player(this.game, 80, 700, 22);
            this.game.physics.enable(this.gems, Phaser.Physics.ARCADE);
            var lvl = new Level1();
            var x = MyGame.Describer.describe(lvl);
            console.log(x);
        };
        Level1.prototype.update = function () {
            this.background.tilePosition.y += 1.5;
            if (this.totalGem < this.maxGems) {
                this.gemGroup.add(this.gems.releaseGems());
                this.totalGem++;
            }
            else {
            }
            this.game.physics.arcade.collide(this.gems, this.player);
            this.game.debug.spriteInfo(this.gems, 54, 54);
            this.gems.position.y += 1;
            this.gems.angle += 3;
        };
        Level1.prototype.getRandomInt = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        };
        return Level1;
    }(Phaser.State));
    MyGame.Level1 = Level1;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MainMenu.prototype.create = function () {
            this.music = this.add.audio('titleMusic');
            this.music.play();
            this.background = this.add.sprite(0, 0, 'titlepage');
            this.background.width = 360;
            this.background.height = 640;
            this.background.alpha = 0;
            this.logo = this.add.sprite(this.world.centerX, -300, 'logo');
            this.logo.width = 196, 2;
            this.logo.height = 87, 4;
            this.logo.anchor.setTo(0.5, 0.5);
            this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
            this.title = this.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);
            this.startChecking();
            this.btnStart = this.add.sprite(this.world.centerX, -600, 'btn');
        };
        MainMenu.prototype.startChecking = function () {
            this.title.onComplete.add(this.onComplete, this, 0, {
                message: "Veuillez renseigner votre identifiant",
                title: "Authentification",
                inputName: "username"
            });
        };
        MainMenu.prototype.onComplete = function (title, context, data) {
            var _this = this;
            navigator.notification.prompt(data.message, function (results) {
                _this.callback(results, data.inputName);
            }, data.title, data.btn, data.text);
            console.log('prompt passed');
        };
        MainMenu.prototype.callback = function (results, inputName) {
            var _this = this;
            console.log(inputName);
            if (inputName === "username") {
                this.username = results.input1;
                this.onComplete(this.title, 0, {
                    message: "Veuillez renseigner votre password",
                    title: "Authentification",
                    inputName: "password"
                });
                this.username = results.input1;
            }
            else {
                this.password = results.input1;
                var res = fetch('http://51.15.237.59:8080/auth', {
                    method: 'POST',
                    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        username: this.username,
                        password: this.password
                    })
                })
                    .then(function (response) {
                    console.log(response);
                    response.json();
                })
                    .then(function (responseData) {
                    console.log(responseData);
                    return _this.fadeOut();
                })["catch"](function (err) {
                    console.log(err);
                    _this.startChecking();
                });
                console.log(res);
            }
        };
        MainMenu.prototype.fadeOut = function () {
            this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
            var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Bounce.In, true);
            tween.onComplete.add(this.getMain, this);
        };
        MainMenu.prototype.getMain = function () {
            this.game.state.start('ParentAdmin', true, false);
        };
        return MainMenu;
    }(Phaser.State));
    MyGame.MainMenu = MainMenu;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var ParentAdmin = (function (_super) {
        __extends(ParentAdmin, _super);
        function ParentAdmin() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ParentAdmin.prototype.preload = function () {
            this.style = {
                font: "18px Arial",
                fill: '#FFF',
                wordWrap: true,
                align: "center"
            };
        };
        ParentAdmin.prototype.create = function () {
            this.background = this.add.sprite(0, 0, 'titlepage');
            this.background.width = 360;
            this.background.height = 640;
            this.panel = this.add.image(50, 100, 'panel', 'Panel');
            this.panel.width = 260;
            this.panel.height = 390;
            this.panelTitle = this.add.image(80, 80, 'panelTitle', 'Panel');
            this.panelTitle.width = 200;
            this.panelTitle.height = 50;
            this.btnChildAccount = this.add.image(100, 170, 'btnChildAccount', 'Button');
            this.btnChildAccount.width = 160;
            this.btnChildAccount.height = 60;
            this.btnResult = this.add.image(100, 240, 'btnResult', 'Button');
            this.btnResult.width = 160;
            this.btnResult.height = 60;
            this.btnAddOn = this.add.image(100, 320, 'btnAddOn', 'Button');
            this.btnAddOn.width = 160;
            this.btnAddOn.height = 60;
            this.btnAddChild = this.add.image(85, 400, 'btnAddChild', 'Button');
            this.btnAddChild.width = 190;
            this.btnAddChild.height = 60;
            this.btnBottom = this.add.image(45, 520, 'btnBottom', 'ButtonPlay');
            this.btnBottom.width = 270;
            this.btnBottom.height = 60;
            this.clickButtonPlay = this.game.add.button(135, 500, 'btnPlay', this.startAddition, this);
            this.clickButtonPlay.width = 90;
            this.clickButtonPlay.height = 90;
        };
        ParentAdmin.prototype.update = function () {
        };
        ParentAdmin.prototype.startAddition = function () {
            this.state.start('Addition', true, false);
        };
        ParentAdmin.prototype.makeBtn = function (x, y, name, group, text) {
        };
        return ParentAdmin;
    }(Phaser.State));
    MyGame.ParentAdmin = ParentAdmin;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y, frame) {
            var _this = _super.call(this, game, x, y, 'player', frame) || this;
            _this.game.physics.arcade.enableBody(_this);
            _this.width = 43.5;
            _this.height = 55;
            _this.anchor.setTo(.5);
            _this.animations.add('runR', [9, 10, 11], 60);
            _this.animations.add('runL', [24, 25, 26], 60);
            _this.animations.add('forward', [22], 90);
            _this.animations.add('backward', [0], 90);
            _this.animations.add('jump', [3, 8], 70);
            game.add.existing(_this);
            return _this;
        }
        Player.prototype.update = function () {
            this.body.velocity.y = 0;
            this.body.velocity.x = 0;
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
                this.body.velocity.x = -150;
                this.animations.play('runL');
                if (this.scale.x == 1) {
                    this.scale.x = -1;
                }
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                this.body.velocity.x = 150;
                this.animations.play('runR');
                if (this.scale.x == -1) {
                    this.scale.x = 1;
                }
            }
            else {
                this.animations.frame = 22;
            }
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
                this.body.velocity.y = -100;
                this.animations.play('forward');
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
                this.body.velocity.y = 100;
                this.animations.play('backward');
            }
        };
        return Player;
    }(Phaser.Sprite));
    MyGame.Player = Player;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.ready = false;
            return _this;
        }
        Preloader.prototype.preload = function () {
            this.preloadBar = this.add.sprite(300, 400, 'preloadBar');
            this.load.setPreloadSprite(this.preloadBar);
            this.load.image('titlepage', 'assets/titlepage.jpg');
            this.load.audio('titleMusic', 'assets/title.mp3', true);
            this.load.image('logo', 'assets/logo.png');
            this.load.spritesheet('player', 'assets/player.png', 79.8, 108.3, 27);
            this.load.spritesheet('bg_game', 'assets/bg_game_1.png', 1080, 1920);
            this.load.spritesheet('gem', 'assets/gem.png', 79, 83);
            this.load.image('panel', 'assets/Panel.png');
            this.load.image('btn', 'assets/btn.png');
            this.load.image('panelTitle', 'assets/base.png');
            this.load.image("timebar", "assets/timebar.png");
            this.load.image("btnAddChild", "assets/btnAdmin/add_child.png");
            this.load.image("btnAddOn", "assets/btnAdmin/btn_add_on.png");
            this.load.image("btnBottom", "assets/btnAdmin/btn_bottom_child.png");
            this.load.image("btnChildAccount", "assets/btnAdmin/btn_child_account.png");
            this.load.image("btnResult", "assets/btnAdmin/btn_result.png");
            this.load.image("btnPlay", "assets/btnAdmin/play_btn.png");
            this.load.image('buttonmask', "assets/buttonmask.png");
            this.load.spritesheet("buttons", "assets/buttons.png", 400, 50, 3);
            var db = this.db();
            console.log(db);
        };
        Preloader.prototype.db = function () {
            var dao = new MyGame.AppDao();
            var daoUser = new MyGame.Users(dao);
            var daoSettings = new MyGame.Settings(dao);
            daoUser.createTable()
                .then(function () { return daoSettings.createTable(); })
                .then(function (data) {
                daoSettings.create("Theme", "Paramètrage du thème", 0, "");
                console.log("create", data);
            })
                .then(function () { return daoUser.create("Daryl", "1"); })
                .then(function (data) {
                console.log(data);
            })["catch"](function (err) {
                console.log('Error: ', err);
                console.log(JSON.stringify(err));
            });
        };
        Preloader.prototype.create = function () {
            this.game.state.start('MainMenu');
        };
        return Preloader;
    }(Phaser.State));
    MyGame.Preloader = Preloader;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Server = (function () {
        function Server(params, server, collection) {
            this.params = params;
        }
        Server.prototype.get = function () {
            var _this = this;
            console.log(this.params);
            fetch('http://192.168.42.139:8080/users', {
                method: 'POST',
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    username: this.params.username,
                    password: this.params.password
                })
            })
                .then(function (response) {
                console.log(response);
                response.json();
            })
                .then(function (responseData) {
                console.log(responseData);
                _this.res = responseData;
            })["catch"](function (err) {
                console.log(err);
            });
        };
        return Server;
    }());
    MyGame.Server = Server;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var AppDao = (function () {
        function AppDao() {
            this.db = sqlitePlugin.openDatabase({
                name: 'edu_school.db',
                location: 'default',
                androidDatabaseImplementation: 1
            });
        }
        AppDao.prototype.run = function (sql, args) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.db.transaction(function (tx) {
                    tx.executeSql(sql, args, (function (tx, res) {
                        resolve(res);
                    }), (function (tx, err) {
                        reject(err);
                    }));
                }, (function (err) {
                }));
            });
        };
        AppDao.prototype.get = function (sql, params) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.db.transaction(function (tx) {
                    tx.executeSql(sql, params, (function (tx, res) {
                        console.log(tx, res);
                        resolve(res);
                    }), (function (tx, err) {
                        console.log(tx, err);
                        reject(err);
                    }));
                }, (function (err) {
                    console.log(err);
                }));
            });
        };
        return AppDao;
    }());
    MyGame.AppDao = AppDao;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Settings = (function () {
        function Settings(dao) {
            this.dao = dao;
        }
        Settings.prototype.createTable = function () {
            var sql = "\n            CREATE TABLE IF NOT EXISTS settings (\n                id INTEGER PRIMARY KEY AUTOINCREMENT,\n                name TEXT,\n                description TEXT,\n                isActive INTEGER DEFAULT 0,\n                value TEXT\n                )";
            return this.dao.run(sql);
        };
        Settings.prototype.create = function (name, description, isActive, value) {
            return this.dao.run("INSERT INTO settings (name, description, isActive, value)\n                    VALUES (?, ?, ?, ?)", [name, description, isActive, value]);
        };
        Settings.prototype.update = function (task) {
            var id = task.id, name = task.name, description = task.description, isActive = task.isActive, value = task.value;
            return this.dao.run("UPDATE settings\n                SET name = ?,\n                    description = ?,\n                    isActive = ?,\n                    value = ?\n                WHERE id = ?", [name, description, isActive, value, id]);
        };
        Settings.prototype["delete"] = function (id) {
            return this.dao.run("DELETE FROM settings WHERE id = ?", [id]);
        };
        Settings.prototype.getById = function (id) {
            return this.dao.get("SELECT * FROM settings WHERE id = ?", [id]);
        };
        return Settings;
    }());
    MyGame.Settings = Settings;
})(MyGame || (MyGame = {}));
var MyGame;
(function (MyGame) {
    var Users = (function () {
        function Users(dao) {
            this.dao = dao;
        }
        Users.prototype.createTable = function () {
            var sql = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, idToken TEXT, level INTEGER DEFAULT 1, achievements TEXT DEFAULT '', isPremium INTEGER DEFAULT 0)";
            return this.dao.run(sql, []);
        };
        Users.prototype.create = function (name, idToken, level, achievements, isPremium) {
            return this.dao.run('INSERT INTO users (name, idToken ,level ,achievements, isPremium ) VALUES (?,?,?,?,?)', [name, idToken, level, achievements, isPremium]);
        };
        Users.prototype.updateAchievements = function (achievements, name) {
            return this.dao.run("UPDATE users SET achievements = ? WHERE name = ?", [achievements, name]);
        };
        Users.prototype["delete"] = function (id) {
            return this.dao.run("DELETE FROM users WHERE id = ?", [id]);
        };
        Users.prototype.getById = function (id) {
            return this.dao.run("SELECT * FROM users WHERE id = ?", [id]);
        };
        Users.prototype.getAchievements = function (achievements) {
            return this.dao.run("SELECT achievements FROM users WHERE instr(achievements, ?)", [achievements]);
        };
        Users.prototype.checkTable = function (tableName) {
            return this.dao.run("SELECT count(*) FROM users WHERE type='table' AND name = ?", [tableName]);
        };
        return Users;
    }());
    MyGame.Users = Users;
})(MyGame || (MyGame = {}));
//# sourceMappingURL=game.js.map
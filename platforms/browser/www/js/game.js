var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
System.register("database/AppDAO", [], function (exports_1, context_1) {
    "use strict";
    var AppDao;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            AppDao = (function () {
                function AppDao() {
                    this.db = sqlitePlugin.openDatabase({
                        name: 'edu_school.db',
                        location: 'default',
                        androidDatabaseImplementation: 1
                    });
                }
                AppDao.prototype.run = function (sql, args) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.db.transaction(function (tx) {
                            sql.forEach(function (element) {
                                tx.executeSql(element, [args], (function (tx, res) {
                                    console.log(tx, res);
                                    resolve(res);
                                }), (function (tx, err) {
                                    console.log(tx, err);
                                    reject(err);
                                }));
                            });
                        }, (function (err) {
                            console.log(err);
                        }));
                    });
                };
                return AppDao;
            }());
            exports_1("default", AppDao);
        }
    };
});
System.register("database/Users", [], function (exports_2, context_2) {
    "use strict";
    var Users;
    var __moduleName = context_2 && context_2.id;
    return {
        setters: [],
        execute: function () {
            Users = (function () {
                function Users(dao) {
                    this.dao = dao;
                }
                Users.prototype.createTable = function () {
                    var sql = "\n        CREATE TABLE IF NOT EXISTS users (\n            id INTEGER PRIMARY KEY AUTOINCREMENT,\n            name TEXT,\n            idToken TEXT,\n            level INTEGER DEFAULT 1,\n            achievements TEXT DEFAULT '',\n            isPremium INTEGER DEFAULT 0\n            )";
                    return this.dao.run(sql);
                };
                Users.prototype.create = function (name, idToken, level, achievements, isPremium) {
                    return this.dao.run('INSERT INTO users (name, idToken ,level ,achievements, isPremium ) VALUES (?,?,?,?,?)', [name, idToken, level, achievements, isPremium]);
                };
                Users.prototype.updateName = function (user) {
                    var id = user.id, name = user.name;
                    return this.dao.run("UPDATE users SET name = ? WHERE id = ?", [name, id]);
                };
                Users.prototype["delete"] = function (id) {
                    return this.dao.run("DELETE FROM users WHERE id = ?", [id]);
                };
                return Users;
            }());
            exports_2("default", Users);
        }
    };
});
System.register("database/Settings", [], function (exports_3, context_3) {
    "use strict";
    var Settings;
    var __moduleName = context_3 && context_3.id;
    return {
        setters: [],
        execute: function () {
            Settings = (function () {
                function Settings(dao) {
                    this.dao = dao;
                }
                Settings.prototype.createTable = function () {
                    var sql = "\n        CREATE TABLE IF NOT EXISTS settings (\n            id INTEGER PRIMARY KEY AUTOINCREMENT,\n            name TEXT,\n            description TEXT,\n            isActive INTEGER DEFAULT 0,\n            value TEXT";
                    return this.dao.run(sql);
                };
                Settings.prototype.create = function (name, description, isActive, value) {
                    return this.dao.run("INSERT INTO settings (name, description, isActive, value)\n                VALUES (?, ?, ?, ?)", [name, description, isActive, value]);
                };
                Settings.prototype.update = function (task) {
                    var id = task.id, name = task.name, description = task.description, isActive = task.isActive, value = task.value;
                    return this.dao.run("UPDATE settings\n            SET name = ?,\n                description = ?,\n                isActive = ?,\n                value = ?\n            WHERE id = ?", [name, description, isActive, value, id]);
                };
                Settings.prototype["delete"] = function (id) {
                    return this.dao.run("DELETE FROM settings WHERE id = ?", [id]);
                };
                Settings.prototype.getById = function (id) {
                    return this.dao.get("SELECT * FROM settings WHERE id = ?", [id]);
                };
                return Settings;
            }());
            exports_3("default", Settings);
        }
    };
});
System.register("Preloader", [], function (exports_4, context_4) {
    "use strict";
    var Preloader;
    var __moduleName = context_4 && context_4.id;
    return {
        setters: [],
        execute: function () {
            Preloader = (function (_super) {
                __extends(Preloader, _super);
                function Preloader() {
                    var _this = _super !== null && _super.apply(this, arguments) || this;
                    _this.ready = false;
                    return _this;
                }
                Preloader.prototype.preload = function () {
                    this.preloadBar = this.add.sprite(300, 400, 'preloadBar');
                    this.load.setPreloadSprite(this.preloadBar);
                    this.load.image('titlepage', 'assets/titlepage.jpg');
                    this.load.audio('titleMusic', 'assets/title.mp3', true);
                    this.load.image('logo', 'assets/logo.png');
                    this.load.spritesheet('player', 'assets/player.png', 79.8, 108.3, 27);
                    this.load.spritesheet('bg_game', 'assets/bg_game_1.png', 1080, 1920);
                    this.load.spritesheet('gem', 'assets/gem.png', 79, 83);
                };
                Preloader.prototype.create = function () {
                    this.game.state.start('MainMenu');
                };
                return Preloader;
            }(Phaser.State));
            exports_4("default", Preloader);
        }
    };
});
System.register("MainMenu", [], function (exports_5, context_5) {
    "use strict";
    var MainMenu;
    var __moduleName = context_5 && context_5.id;
    return {
        setters: [],
        execute: function () {
            MainMenu = (function (_super) {
                __extends(MainMenu, _super);
                function MainMenu() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                MainMenu.prototype.create = function () {
                    this.music = this.add.audio('titleMusic');
                    this.music.play();
                    this.background = this.add.sprite(0, 0, 'titlepage');
                    this.background.width = 360;
                    this.background.height = 640;
                    this.background.alpha = 0;
                    this.logo = this.add.sprite(this.world.centerX, -300, 'logo');
                    this.logo.width = 196, 2;
                    this.logo.height = 87, 4;
                    this.logo.anchor.setTo(0.5, 0.5);
                    this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
                    this.title = this.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);
                    this.startChecking;
                    this.btnStart = this.add.sprite(this.world.centerX, -600, 'btn');
                    console.log(this.onComplete);
                };
                MainMenu.prototype.startChecking = function () {
                    this.title.onComplete.add(this.onComplete, this, 0, {
                        message: "Veuillez renseigner votre identifiant",
                        title: "Authentification",
                        inputName: "username"
                    });
                };
                MainMenu.prototype.onComplete = function (title, context, data) {
                    var _this = this;
                    navigator.notification.prompt(data.message, function (results) {
                        _this.callback(results, data.inputName);
                    }, data.title, data.btn, data.text);
                    return this.callback;
                };
                MainMenu.prototype.callback = function (results, inputName) {
                    if (inputName = "username") {
                        this.username = results.input1;
                        this.title.onComplete.add(this.onComplete, this, 0, {
                            message: "Veuillez renseigner votre password",
                            title: "Authentification",
                            inputName: "password"
                        });
                        console.log(this.username);
                        return results.input1;
                    }
                    else {
                        this.password = results.input1;
                        console.log(this.password);
                        return;
                    }
                };
                MainMenu.prototype.fadeOut = function () {
                    this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
                    var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Bounce.In, true);
                    tween.onComplete.add(this.getMain, this);
                };
                MainMenu.prototype.getMain = function () {
                    this.game.state.start('ParentAdmin', true, false);
                };
                return MainMenu;
            }(Phaser.State));
            exports_5("default", MainMenu);
        }
    };
});
System.register("Player", [], function (exports_6, context_6) {
    "use strict";
    var Player;
    var __moduleName = context_6 && context_6.id;
    return {
        setters: [],
        execute: function () {
            Player = (function (_super) {
                __extends(Player, _super);
                function Player(game, x, y, frame) {
                    var _this = _super.call(this, game, x, y, 'player', frame) || this;
                    _this.game.physics.arcade.enableBody(_this);
                    _this.width = 43.5;
                    _this.height = 55;
                    _this.anchor.setTo(.5);
                    _this.animations.add('runR', [9, 10, 11], 60);
                    _this.animations.add('runL', [24, 25, 26], 60);
                    _this.animations.add('forward', [22], 90);
                    _this.animations.add('backward', [0], 90);
                    _this.animations.add('jump', [3, 8], 70);
                    game.add.existing(_this);
                    return _this;
                }
                Player.prototype.update = function () {
                    this.body.velocity.y = 0;
                    this.body.velocity.x = 0;
                    if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
                        this.body.velocity.x = -150;
                        this.animations.play('runL');
                        if (this.scale.x == 1) {
                            this.scale.x = -1;
                        }
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                        this.body.velocity.x = 150;
                        this.animations.play('runR');
                        if (this.scale.x == -1) {
                            this.scale.x = 1;
                        }
                    }
                    else {
                        this.animations.frame = 22;
                    }
                    if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
                        this.body.velocity.y = -100;
                        this.animations.play('forward');
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
                        this.body.velocity.y = 100;
                        this.animations.play('backward');
                    }
                };
                return Player;
            }(Phaser.Sprite));
            exports_6("default", Player);
        }
    };
});
System.register("Describer", [], function (exports_7, context_7) {
    "use strict";
    var Describer;
    var __moduleName = context_7 && context_7.id;
    return {
        setters: [],
        execute: function () {
            Describer = (function () {
                function Describer() {
                }
                Describer.describe = function (instance) {
                    return Object.getOwnPropertyNames(instance);
                };
                return Describer;
            }());
            exports_7("default", Describer);
        }
    };
});
System.register("Gems", [], function (exports_8, context_8) {
    "use strict";
    var Gems;
    var __moduleName = context_8 && context_8.id;
    return {
        setters: [],
        execute: function () {
            Gems = (function (_super) {
                __extends(Gems, _super);
                function Gems(game, x, y, gemMaxNumber) {
                    var _this = _super.call(this, game, x, y, 'gem', 0) || this;
                    _this.game.physics.arcade.enableBody(_this);
                    _this.width = 29;
                    _this.height = 31;
                    _this.anchor.setTo(.5);
                    _this.gemMaxNumber = gemMaxNumber;
                    _this.body.immovable = true;
                    _this.game.add.existing(_this);
                    return _this;
                }
                Gems.prototype.releaseGems = function () {
                    if (this.gemMaxNumber > this.total) {
                        this.gem = this.game.add.sprite((Math.floor(320)), this.game.world.randomY, 'gem');
                        this.total++;
                    }
                    else {
                    }
                };
                return Gems;
            }(Phaser.Sprite));
            exports_8("default", Gems);
        }
    };
});
System.register("Level1", ["Player", "Describer", "Gems"], function (exports_9, context_9) {
    "use strict";
    var Player_1, Describer_1, Gems_1, Level1;
    var __moduleName = context_9 && context_9.id;
    return {
        setters: [
            function (Player_1_1) {
                Player_1 = Player_1_1;
            },
            function (Describer_1_1) {
                Describer_1 = Describer_1_1;
            },
            function (Gems_1_1) {
                Gems_1 = Gems_1_1;
            }
        ],
        execute: function () {
            Level1 = (function (_super) {
                __extends(Level1, _super);
                function Level1() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                Level1.prototype.create = function () {
                    this.image = this.add.image(0, 0, 'gem');
                    this.background = this.game.add.tileSprite(0, 0, this.game.stage.width, this.game.cache.getImage('bg_game').height, 'bg_game');
                    this.gems = new Gems_1["default"](this.game, this.getRandomInt(10, 320), this.getRandomInt(10, 600), this.maxGems);
                    this.game.debug.spriteInfo(this.gems, 32, 32);
                    this.player = new Player_1["default"](this.game, 80, 700, 22);
                    this.game.physics.enable(this.gems, Phaser.Physics.ARCADE);
                    var lvl = new Level1();
                    var x = Describer_1["default"].describe(lvl);
                    console.log(x);
                };
                Level1.prototype.update = function () {
                    this.background.tilePosition.y += 1.5;
                    if (this.totalGem < this.maxGems) {
                        this.gemGroup.add(this.gems.releaseGems());
                        this.totalGem++;
                    }
                    else {
                    }
                    this.game.physics.arcade.collide(this.gems, this.player);
                    this.game.debug.spriteInfo(this.gems, 54, 54);
                    this.gems.position.y += 1;
                    this.gems.angle += 3;
                };
                Level1.prototype.getRandomInt = function (min, max) {
                    return Math.floor(Math.random() * (max - min + 1) + min);
                };
                return Level1;
            }(Phaser.State));
            exports_9("default", Level1);
        }
    };
});
System.register("Game", ["Preloader", "Boot", "MainMenu", "Level1"], function (exports_10, context_10) {
    "use strict";
    var Preloader_1, Boot_1, MainMenu_1, Level1_1, gameWidth, gameHeight, Game, app;
    var __moduleName = context_10 && context_10.id;
    return {
        setters: [
            function (Preloader_1_1) {
                Preloader_1 = Preloader_1_1;
            },
            function (Boot_1_1) {
                Boot_1 = Boot_1_1;
            },
            function (MainMenu_1_1) {
                MainMenu_1 = MainMenu_1_1;
            },
            function (Level1_1_1) {
                Level1_1 = Level1_1_1;
            }
        ],
        execute: function () {
            console.log(Boot_1["default"]);
            exports_10("gameWidth", gameWidth = window.innerWidth * window.devicePixelRatio);
            exports_10("gameHeight", gameHeight = window.innerHeight * window.devicePixelRatio);
            Game = (function (_super) {
                __extends(Game, _super);
                function Game() {
                    var _this = _super.call(this, 360, 640, Phaser.AUTO, 'content') || this;
                    _this.state.add('Boot', Boot_1["default"], false);
                    _this.state.add('Preloader', Preloader_1["default"], false);
                    _this.state.add('MainMenu', MainMenu_1["default"], false);
                    _this.state.add('Level1', Level1_1["default"], false);
                    _this.state.start('Boot');
                    console.log('passe Game.ts');
                    return _this;
                }
                return Game;
            }(Phaser.Game));
            exports_10("default", Game);
            app = {
                initialize: function () {
                    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
                    console.log('init');
                },
                onDeviceReady: function () {
                    this.receivedEvent('deviceready');
                },
                receivedEvent: function (id) {
                    new Game;
                }
            };
            app.initialize();
        }
    };
});
System.register("Boot", [], function (exports_11, context_11) {
    "use strict";
    var Boot;
    var __moduleName = context_11 && context_11.id;
    return {
        setters: [],
        execute: function () {
            Boot = (function (_super) {
                __extends(Boot, _super);
                function Boot() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                Boot.prototype.init = function () {
                    this.input.maxPointers = 1;
                    this.stage.disableVisibilityChange = true;
                    this.game.physics.startSystem(Phaser.Physics.ARCADE);
                    if (this.game.device.desktop) {
                        this.game.scale.pageAlignHorizontally = true;
                        this.game.scale.pageAlignVertically = true;
                    }
                    else {
                        this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
                    }
                };
                Boot.prototype.preload = function () {
                    this.load.image('preloadBar', 'assets/loader.png');
                };
                Boot.prototype.create = function () {
                    this.game.state.start('Preloader');
                };
                return Boot;
            }(Phaser.State));
            exports_11("default", Boot);
        }
    };
});
var MyGame;
(function (MyGame) {
    var Emitter = (function (_super) {
        __extends(Emitter, _super);
        function Emitter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Emitter;
    }(Phaser.Group));
    MyGame.Emitter = Emitter;
})(MyGame || (MyGame = {}));
System.register("ParentAdmin", [], function (exports_12, context_12) {
    "use strict";
    var ParentAdmin;
    var __moduleName = context_12 && context_12.id;
    return {
        setters: [],
        execute: function () {
            ParentAdmin = (function (_super) {
                __extends(ParentAdmin, _super);
                function ParentAdmin() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                ParentAdmin.prototype.create = function () {
                };
                ParentAdmin.prototype.update = function () {
                };
                return ParentAdmin;
            }(Phaser.State));
            exports_12("default", ParentAdmin);
        }
    };
});
//# sourceMappingURL=game.js.map